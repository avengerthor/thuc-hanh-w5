import 'package:flutter_test/flutter_test.dart';
import 'package:exercise_plan_management/controllers/plan_controller.dart';

void main() {
  test('Test addNewPlan', () {
    var controller = PlanController();
    controller.addNewPlan('A');

    expect(1, controller.plans.length);
    expect('A', controller.plans.first.name);
  });

  test('Test addNewPlan with Vietnamese name', () {
    var controller = PlanController();
    controller.addNewPlan('Tạo giao diện ứng dụng bán hàng online');

    expect(1, controller.plans.length);
    expect('Tạo giao diện ứng dụng bán hàng online', controller.plans.first.name);
  });

}